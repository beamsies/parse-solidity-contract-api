# Node.js app that parses Solidity code

## Getting started

1. clone the repo `git clone https://gitlab.com/beamsies/parse-solidity-contract-api`

2. install dependencies `yarn install` or just `yarn`

3. develop locally with `yarn dev`

4. build project `yarn build`

5. run tests `yarn jest` or `yarn test`

## Technologies used

- Node.js
- Express.js
- TypeScript
- Jest for testing

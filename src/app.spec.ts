import * as path from 'path';
import { readFileSync } from 'fs';
import supertest from 'supertest';
import { app, server } from './app';

describe('app.ts', () => {
  afterEach(() => {
    server.close();
  });
  it('should say hello at root', (done) => {
    supertest(app)
      .get('/')
      .expect(200)
      .then((response) => {
        expect(response.text).toEqual('Hello World');
        done();
      });
  });

  it('should parse solidity code input', (done) => {
    const contract = readFileSync(
      path.resolve(__dirname, '../fixtures/contracts/SplitMain.sol'),
    );

    const parsedContract = JSON.parse(
      readFileSync(
        path.resolve(__dirname, '../fixtures/parsed/SplitMainReport.json'),
        'utf-8',
      ),
    );

    supertest(app)
      .post('/analyze')
      .send({ code: contract.toString() })
      .expect(200, parsedContract, done);
  });

  it('should handle empty code input', (done) => {
    const contract = readFileSync(
      path.resolve(__dirname, '../fixtures/contracts/EmptyContract.sol'),
    );

    supertest(app)
      .post('/analyze')
      .send({ code: contract.toString() })
      .expect(200, { imports: [], contracts: [] }, done);
  });

  it('should handle invalid code input (contract with syntax errors)', (done) => {
    const contract = readFileSync(
      path.resolve(__dirname, '../fixtures/contracts/InvalidContract.sol'),
    );

    supertest(app)
      .post('/analyze')
      .send({ code: contract.toString() })
      .expect(400, { error: 'Parser Error' }, done);
  });

  it('should handle invalid code format input', (done) => {
    const contract = readFileSync(
      path.resolve(__dirname, '../fixtures/contracts/SomeInvalidCode.txt'),
    );

    supertest(app)
      .post('/analyze')
      .send({ code: contract.toString() })
      .expect(500, { error: 'Error' }, done);
  });
});

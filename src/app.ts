import express from 'express';
import * as solParser from '@solidity-parser/parser';

export const app = express();
const port = 3001;

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.post('/analyze', (req, res) => {
  const code = req.body.code;

  try {
    const result = solParser.parse(code);

    const data: {
      imports: string[];
      contracts: string[];
    } = {
      imports: [],
      contracts: [],
    };

    result.children.map((child) => {
      if (child.type === 'ImportDirective') {
        data.imports.push(child.path);
      }
      if (child.type === 'ContractDefinition') {
        data.contracts.push(child.name);
      }
    });

    return res.json(data);
  } catch (e) {
    if (e instanceof solParser.ParserError) {
      return res.status(400).json({ error: 'Parser Error' });
    }

    return res.status(500).json({ error: 'Error' });
  }
});

export const server = app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port ${port}`);
});
